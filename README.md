# Weatherstation

## Hardware 

* TTGO T-DISPLAY ESP32 module: http://www.lilygo.cn/prod_view.aspx?TypeId=50033&Id=1126&FId=t3:50033:3
* Bosch BME-280 sensor on breakout board

Connection: 
* BME-280 VCC to 3.3V on ESP32
* BME-280 GND to GND on ESP32
* BME-280 SDA to Pin 21 on ESP32
* BME-280 SCL to Pin 22 on ESP32

## Software
- Project done with PlatformIO. 
- Before compilation you need to adapt the library for the TFT Display: 
    * in `.pio/libdeps/ttgo-display/User_Setup.h` uncomment `#define ST7789_DRIVER` , comment all other Drivers
    * in `.pio/libdeps/ttgo-display/User_Select_Setup.h` uncomment `#include <User_Setups/Setup25_TTGO_T_Display.h>`, comment all other setups

