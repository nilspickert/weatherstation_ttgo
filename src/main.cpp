#include <Arduino.h>
#include <WiFi.h>
#include "SPI.h"
#include <TFT_eSPI.h>
#include "time.h"
#include "Free_Fonts.h"
#include <ArduinoJson.h>
#include <HTTPClient.h>
#include <Wire.h>
#include <Adafruit_Sensor.h>
#include <Adafruit_BME280.h>
#include <WebServer.h>
#include <DNSServer.h>
#include <WiFiManager.h> // https://github.com/tzapu/WiFiManager WiFi Configuration Magic
#include <ArduinoOTA.h>
// MQTT
#define MQTT_MAX_PACKET_SIZE 1844
#include <PubSubClient.h>

const char *ntpServer = "pool.ntp.org";
const long gmtOffset_sec = 3600;
const int daylightOffset_sec = 3600;

char ipaddress[16];
bool updated = false;
bool updated_sensor = false;

#define REGION 3 // Region for DWD JSON Data
char Birke[4];
char Esche[4];
char Graeser[4];

// WiFi Manager
WiFiManager wifiManager;

TFT_eSPI tft = TFT_eSPI(135, 240);

Adafruit_BME280 bme;

HTTPClient http;

// MQTT Client
const char *mqtt_server = "astropad";
WiFiClient espClient;
PubSubClient client(espClient);
unsigned long lastMsg = 0;
#define MSG_BUFFER_SIZE (200)
char msg[MSG_BUFFER_SIZE];

// MQTT Functions
void callback(char *topic, byte *payload, unsigned int length)
{
  Serial.print("Message arrived [");
  Serial.print(topic);
  Serial.print("] ");
  for (int i = 0; i < length; i++)
  {
    Serial.print((char)payload[i]);
  }
  Serial.println();

  // Switch on the LED if an 1 was received as first character
  if ((char)payload[0] == '1')
  {
    digitalWrite(BUILTIN_LED, LOW); // Turn the LED on (Note that LOW is the voltage level
    // but actually the LED is on; this is because
    // it is active low on the ESP-01)
  }
  else
  {
    digitalWrite(BUILTIN_LED, HIGH); // Turn the LED off by making the voltage HIGH
  }
}

void reconnect()
{
  // Loop until we're reconnected
  while (!client.connected())
  {
    Serial.print("Attempting MQTT connection...");
    // Create a random client ID
    String clientId = "ESP8266Client-";
    clientId += String(random(0xffff), HEX);
    // Attempt to connect
    if (client.connect(clientId.c_str()))
    {
      Serial.println("connected");
      // Once connected, publish an announcement...
      client.publish("outTopic", "hello world");
      // ... and resubscribe
      client.subscribe("inTopic");
    }
    else
    {
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
      // Wait 5 seconds before retrying
      delay(5000);
    }
  }
}

void setup()
{
  Serial.begin(9600);
  Serial.println();
  Serial.println();
  Serial.println("Booting");

  wifiManager.autoConnect("Wetter01");

  tft.init();
  tft.fillScreen(TFT_BLACK);

  // Over the Air Updates
  ArduinoOTA.setHostname("Wetter01");

  ArduinoOTA
      .onStart([]() {
        String type;
        if (ArduinoOTA.getCommand() == U_FLASH)
          type = "sketch";
        else // U_SPIFFS
          type = "filesystem";

        // NOTE: if updating SPIFFS this would be the place to unmount SPIFFS using SPIFFS.end()
        Serial.println("Start updating " + type);
      })
      .onEnd([]() {
        Serial.println("\nEnd");
      })
      .onProgress([](unsigned int progress, unsigned int total) {
        Serial.printf("Progress: %u%%\r", (progress / (total / 100)));
      })
      .onError([](ota_error_t error) {
        Serial.printf("Error[%u]: ", error);
        if (error == OTA_AUTH_ERROR)
          Serial.println("Auth Failed");
        else if (error == OTA_BEGIN_ERROR)
          Serial.println("Begin Failed");
        else if (error == OTA_CONNECT_ERROR)
          Serial.println("Connect Failed");
        else if (error == OTA_RECEIVE_ERROR)
          Serial.println("Receive Failed");
        else if (error == OTA_END_ERROR)
          Serial.println("End Failed");
      });

  ArduinoOTA.begin();

  bool status;

  status = bme.begin(0x76);
  if (!status)
  {
    Serial.println("Could not find a valid BME280 sensor, check wiring!");
    while (1)
      ;
  }

  // Inital load of pollen data
  http.useHTTP10(true);
  http.begin("https://opendata.dwd.de/climate_environment/health/alerts/s31fg.json");
  http.GET();

  // // Parse response
  DynamicJsonDocument doc(24576);
  DeserializationError error = deserializeJson(doc, http.getStream());

  if (error)
  {
    Serial.print(F("deserializeJson() failed: "));
    Serial.println(error.f_str());
    return;
  }

  // // Disconnect
  http.end();
  // Serial.println(doc["content"][3]["Pollen"]["Birke"]["today"].as<String>());
  strcpy(Birke, doc["content"][REGION]["Pollen"]["Birke"]["today"]);
  strcpy(Esche, doc["content"][REGION]["Pollen"]["Esche"]["today"]);
  strcpy(Graeser, doc["content"][REGION]["Pollen"]["Graeser"]["today"]);
  Serial.print("Birke : ");
  Serial.println(Birke);
  Serial.print("Esche : ");
  Serial.println(Esche);
  Serial.print("Graeser : ");
  Serial.println(Graeser);

  // Init and get the time
  configTime(gmtOffset_sec, daylightOffset_sec, ntpServer);

  // Start MQTT
  client.setServer(mqtt_server, 1883);
  client.setCallback(callback);
}

void loop()
{
  // put your main code here, to run repeatedly:
  // OTA
  ArduinoOTA.handle();

  // Get time from NTP
  tm local;
  getLocalTime(&local);

  // MQTT
  if (!client.connected())
  {
    reconnect();
  }
  client.loop();

  // once every hour get pollen data from DWD
  if ((local.tm_min == 0) && (!updated))
  {
    // // Send request
    http.useHTTP10(true);
    http.begin("https://opendata.dwd.de/climate_environment/health/alerts/s31fg.json");
    http.GET();

    // // Parse response
    DynamicJsonDocument doc(24576);
    DeserializationError error = deserializeJson(doc, http.getStream());

    if (error)
    {
      Serial.print(F("deserializeJson() failed: "));
      Serial.println(error.f_str());
      return;
    }

    // // Disconnect
    http.end();
    // Serial.println(doc["content"][3]["Pollen"]["Birke"]["today"].as<String>());
    strcpy(Birke, doc["content"][REGION]["Pollen"]["Birke"]["today"]);
    strcpy(Esche, doc["content"][REGION]["Pollen"]["Esche"]["today"]);
    strcpy(Graeser, doc["content"][REGION]["Pollen"]["Graeser"]["today"]);
    Serial.print("Birke : ");
    Serial.println(Birke);
    Serial.print("Esche : ");
    Serial.println(Esche);
    Serial.print("Graeser : ");
    Serial.println(Graeser);
    updated = true;
  }
  if ((local.tm_min != 0) && (updated))
    updated = false;

  // Every 10 seconds read BME280 and display values
  if ((local.tm_sec % 10 == 0) && (!updated_sensor))
  {

    tft.fillScreen(TFT_BLACK);
    tft.setFreeFont(FM9);
    tft.setCursor(20, 20, 4);
    tft.setTextColor(TFT_DARKGREY);
    //tft.println(&local,"%d.%m.%y\n %H:%M:%S" );
    tft.println(&local, "%d.%m.%y");
    tft.setCursor(35, 40, 4);
    tft.setTextColor(TFT_LIGHTGREY);
    tft.println(&local, "%H:%M");

    tft.setCursor(35, 70);
    tft.print(bme.readTemperature());
    tft.println(" °C");

    tft.setCursor(13, 90);
    tft.print(bme.readPressure() / 100.0F);
    tft.println(" hPa");

    tft.setCursor(35, 110);
    tft.print(bme.readHumidity());
    tft.println(" %");

    // Birkenpollen

    tft.setCursor(35, 140);
    if (strcmp(Birke, "0-1") == 0)
    {
      tft.setTextColor(TFT_DARKGREEN);
      tft.print("Birke");
    }
    else if (strcmp(Birke, "1") == 0)
    {
      tft.setTextColor(TFT_GREEN);
      tft.print("Birke");
    }
    else if (strcmp(Birke, "1-2") == 0)
    {
      tft.setTextColor(TFT_GREENYELLOW);
      tft.print("Birke");
    }
    else if (strcmp(Birke, "2") == 0)
    {
      tft.setTextColor(TFT_YELLOW);
      tft.print("Birke");
    }
    else if (strcmp(Birke, "2-3") == 0)
    {
      tft.setTextColor(TFT_ORANGE);
      tft.print("Birke");
    }
    else if (strcmp(Birke, "3") == 0)
    {
      tft.setTextColor(TFT_RED);
      tft.print("Birke");
    }

    // Eschen Pollen
    tft.setCursor(35, 160);
    if (strcmp(Esche, "0-1") == 0)
    {
      tft.setTextColor(TFT_DARKGREEN);
      tft.print("Esche");
    }
    else if (strcmp(Esche, "1") == 0)
    {
      tft.setTextColor(TFT_GREEN);
      tft.print("Esche");
    }
    else if (strcmp(Esche, "1-2") == 0)
    {
      tft.setTextColor(TFT_GREENYELLOW);
      tft.print("Esche");
    }
    else if (strcmp(Esche, "2") == 0)
    {
      tft.setTextColor(TFT_YELLOW);
      tft.print("Esche");
    }
    else if (strcmp(Esche, "2-3") == 0)
    {
      tft.setTextColor(TFT_ORANGE);
      tft.print("Eschhe");
    }
    else if (strcmp(Esche, "3") == 0)
    {
      tft.setTextColor(TFT_RED);
      tft.print("Esche");
    }

    // Graeser Pollen
    tft.setCursor(30, 180);
    if (strcmp(Graeser, "0-1") == 0)
    {
      tft.setTextColor(TFT_DARKGREEN);
      tft.print("Graeser");
    }
    else if (strcmp(Graeser, "1") == 0)
    {
      tft.setTextColor(TFT_GREEN);
      tft.print("Graeser");
    }
    else if (strcmp(Graeser, "1-2") == 0)
    {
      tft.setTextColor(TFT_GREENYELLOW);
      tft.print("Graeser");
    }
    else if (strcmp(Graeser, "2") == 0)
    {
      tft.setTextColor(TFT_YELLOW);
      tft.print("Graeser");
    }
    else if (strcmp(Graeser, "2-3") == 0)
    {
      tft.setTextColor(TFT_ORANGE);
      tft.print("Graeser");
    }
    else if (strcmp(Esche, "3") == 0)
    {
      tft.setTextColor(TFT_RED);
      tft.print("Graeser");
    }

    // Serial output BME
    Serial.print("Temperature = ");
    Serial.print(bme.readTemperature());
    Serial.println(" *C");

    Serial.print("Pressure = ");
    Serial.print(bme.readPressure() / 100.0F);
    Serial.println(" hPa");

    Serial.print("Humidity = ");
    Serial.print(bme.readHumidity());
    Serial.println(" %");

    snprintf(msg, MSG_BUFFER_SIZE, "{\"Temperature\": %.2f, \"Pressure\": %.2f, \"Humidity\": %.2f }", bme.readTemperature(), bme.readPressure()/100.0F, bme.readHumidity());
    client.publish("wetter/bme280", msg);
    updated_sensor = true;
  }
  if ((local.tm_sec % 10 != 0) && (updated_sensor))
    updated_sensor = false;
}